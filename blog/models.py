from django.db import models


# Create your models here.
class Article(models.Model):
    titre = models.CharField(max_length=100)
    auteur = models.CharField(max_length=100)
    datePublication = models.DateField()
    texte = models.TextField()


class Commentaire(models.Model):
    auteur = models.ForeignKey(
        'Auteur',
        on_delete=models.CASCADE
    )
    datePublication = models.DateField()
    texte = models.TextField()
    article = models.ForeignKey(
        'Article',
        on_delete=models.CASCADE
    )


class Auteur(models.Model):
    nom = models.CharField(max_length=100)
    prenom = models.CharField(max_length=100)
